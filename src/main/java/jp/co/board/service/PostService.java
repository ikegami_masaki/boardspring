package jp.co.board.service;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.board.dto.PostDto;
import jp.co.board.entity.Post;
import jp.co.board.form.SearchForm;
import jp.co.board.mapper.PostMapper;

@Service
public class PostService {

	@Autowired
	private PostMapper postMapper;

	// 新規投稿
	public int insert(PostDto postDto) {
		int count = postMapper.insert(postDto);
		return count;
	}

	// すべての投稿を取得する
	public List<PostDto> getPosts(SearchForm form) {

		// 検索するための文字列を取得
		String category = form.getCategory();
		String startDate = form.getStartDate();
		String endDate = form.getEndDate();
		String endDateSave = null;

		// 時間の初期値を設定
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		if (startDate == null || startDate.isEmpty()) {
			Date date = Date.valueOf("2018-08-01");
			startDate = date.toString() + " 00:00:00";
		}
		if (endDate == null || endDate.isEmpty()) {
			java.util.Date date = new java.util.Date();
			String dateStr = sdf.format(date);
			endDate = dateStr + " 23:59:59";
		} else {

			endDateSave = form.getEndDate();
			endDate += " 23:59:59";
		}

		// 文字列を再設定
		form.setCategory(category);
		form.setStartDate(startDate);
		form.setEndDate(endDate);

		List<Post> posts = postMapper.getPosts(form);
		List<PostDto> ret = convertToDto(posts);

		// *で検索した場合
		form.setEndDate(endDateSave);

		return ret;
	}

	public int delete(int id) {
		int count = postMapper.delete(id);
		return count;
	}

	// データベースからの検索結果をDtoに詰め替える
	private List<PostDto> convertToDto(List<Post> entity) {
		List<PostDto> ret = new LinkedList<PostDto>();

		for (Post post : entity) {
			PostDto dto = new PostDto();
			BeanUtils.copyProperties(post, dto);
			ret.add(dto);
		}

		return ret;
	}
}

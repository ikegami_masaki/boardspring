package jp.co.board.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.board.dto.CommentDto;
import jp.co.board.entity.Comment;
import jp.co.board.mapper.CommentMapper;

@Service
public class CommentService {

	@Autowired
	private CommentMapper commentMapper;

	public int insert(CommentDto dto) {
		int count = commentMapper.insert(dto);
		return count;
	}

	public List<CommentDto> getComments(){
		List<Comment> entity = commentMapper.getComments();
		List<CommentDto> ret = convertToDto(entity);

		return ret;
	}

	public int delete(int id) {
		int count = commentMapper.delete(id);
		return count;
	}

	private List<CommentDto> convertToDto(List<Comment> entity) {
		List<CommentDto> ret = new LinkedList<CommentDto>();

		for (Comment comment : entity) {
			CommentDto dto = new CommentDto();
			BeanUtils.copyProperties(comment, dto);
			ret.add(dto);
		}
		return ret;
	}
}

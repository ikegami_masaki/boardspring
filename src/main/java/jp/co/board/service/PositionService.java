package jp.co.board.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.board.dto.PositionDto;
import jp.co.board.entity.Position;
import jp.co.board.mapper.PositionMapper;

@Service
public class PositionService {

	@Autowired
	private PositionMapper positionMapper;

	public List<PositionDto> getPositions() {
		List<Position> positions = positionMapper.getPositions();
		List<PositionDto> positionDto = convertToDto(positions);
		return positionDto;

	}

	private List<PositionDto> convertToDto(List<Position> positionList) {
		List<PositionDto> resultList = new LinkedList<PositionDto>();
		for (Position position : positionList) {
			PositionDto dto = new PositionDto();
			BeanUtils.copyProperties(position, dto);
			resultList.add(dto);
		}
		return resultList;
	}

}

package jp.co.board.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.board.dto.BranchDto;
import jp.co.board.entity.Branch;
import jp.co.board.mapper.BranchMapper;

@Service
public class BranchService {

	@Autowired
	private BranchMapper branchMapper;

	public List<BranchDto> getBranches(){
		List<Branch> branchList = branchMapper.getBranches();
		List<BranchDto> branchDto = convertToDto(branchList);
		return branchDto;
	}

	// privae --------------------------------------
	/**
	 * @param entity.BranchのList型
	 *
	 * @return entity.Branchのリストからdto.BaranchDtoのリストに移し変えた値を返す
	 * */

	private List<BranchDto> convertToDto(List<Branch> branchList) {
		List<BranchDto> resultList = new LinkedList<BranchDto>();
		for (Branch branch : branchList) {
			BranchDto dto = new BranchDto();
			BeanUtils.copyProperties(branch, dto);
			resultList.add(dto);
		}
		return resultList;
	}

}

package jp.co.board.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.board.dto.UserDto;
import jp.co.board.entity.User;
import jp.co.board.mapper.UserMapper;
import jp.co.board.utils.CipherUtil;

@Service
public class UserService {

	@Autowired
	private UserMapper userMapper;

	public int insert(UserDto userDto) {
		User user = new User();
		BeanUtils.copyProperties(userDto, user );

		// パスワードをハッシュか
		String password = CipherUtil.encrypt(user.getPassword());
		// 停止処理をデフォルトで解除
		user.setIsDeleted(0);

		user.setPassword(password);
		System.out.println("ghjkl;");

		int count = userMapper.insert(user);
		return count;
	}

	/**
	 * @param loginId ログインID
	 * @param password パスワード
	 *
	 * @return 特定のユーザー
	 * */
	public UserDto getUser(String loginId, String password) {

		password = CipherUtil.encrypt(password);

		User user = userMapper.getUser(loginId, password);
		UserDto userDto = new UserDto();
		if (user != null) {

			BeanUtils.copyProperties(user, userDto);
			return userDto;
		} else {

			return null;
		}
	}

	/**
	 * @param id ユーザーのID
	 * @return 特定のユーザー
	 * */
	public UserDto getUserById (int id) {
		User user = userMapper.getUserById(id);
		UserDto dto = new UserDto();
		BeanUtils.copyProperties(user, dto);
		return dto;
	}

	/**
	 * @return ユーザーすべて
	 * */
	public List<UserDto> getUsers() {

		List<User> entity = userMapper.getUsers();
		List<UserDto> ret = convertToDto(entity);

		return ret;
	}

	/**
	 * @return 編集できたユーザー数
	 * */
	public int update(UserDto dto) {

		// パスワードを変更するのみに制限
		if (dto.getPassword().isEmpty() == false) {
			String password = CipherUtil.encrypt(dto.getPassword());
			dto.setPassword(password);
		}

		int count = userMapper.update(dto);
		return count;
	}

	/**
	 * @param userId ユーザーのID
	 * @param deleteNum 変更後のis_deletedカラムの値
	 *
	 * @return 変更したレコード数
	 * */
	public int delete(int userId, int deleteNum)  {

		int count =  userMapper.delete(userId, deleteNum);
		return count;
	}



	private List<UserDto> convertToDto(List<User> entity) {

		List<UserDto> ret = new LinkedList<UserDto>();

		for (User user : entity) {
			UserDto dto = new UserDto();
			BeanUtils.copyProperties(user, dto);
			ret.add(dto);
		}
		return ret;
	}


}

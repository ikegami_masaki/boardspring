package jp.co.board.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.board.dto.CommentDto;
import jp.co.board.dto.PostDto;
import jp.co.board.form.CommentForm;
import jp.co.board.form.SearchForm;
import jp.co.board.service.CommentService;
import jp.co.board.service.PostService;

@Controller
public class TopController {
	@Autowired
	private PostService postService;
	@Autowired
	private CommentService commentService;

	@RequestMapping(value="/", method=RequestMethod.GET)
	public String index(Model model,@ModelAttribute SearchForm searchForm, HttpServletRequest req){

		HttpSession session = req.getSession();
		List<String> messages = (List<String>) session.getAttribute("errorMessages");

		// コメント用のフォーム
		CommentForm commentForm = new CommentForm();
		model.addAttribute("commentForm", commentForm);

		// 投稿情報、コメントを取得
		List<PostDto> posts = postService.getPosts(searchForm);
		List<CommentDto> comments =commentService.getComments();

		model.addAttribute("errorMessages", messages);
		session.setAttribute("errorMessages", null);
		model.addAttribute("searchForm", searchForm);
		model.addAttribute("comments", comments);
		model.addAttribute("posts", posts);
		return "index";
	}

}

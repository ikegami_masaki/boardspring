package jp.co.board.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.board.dto.PostDto;
import jp.co.board.dto.UserDto;
import jp.co.board.form.PostForm;
import jp.co.board.form.validationGroup.PostFormValidation;
import jp.co.board.service.PostService;

@Controller
@RequestMapping(value="/post")
public class PostController {

	@Autowired
	private PostService postService;

	// 新規投稿画面表示
	@RequestMapping(value="/new", method=RequestMethod.GET)
	public String newPost(Model model) {

		model.addAttribute("postForm", new PostForm());
		return "postForm";
	}

	// 新規投稿処理
	@RequestMapping(value="/new", method=RequestMethod.POST)
	public String newPost(@ModelAttribute PostForm form, HttpSession session, Model model) {
		UserDto user = (UserDto) session.getAttribute("loginUser");

		List<String> messages = new ArrayList<String>();

		if (PostFormValidation.isValid(form, messages)) {

			PostDto postDto = new PostDto();
			BeanUtils.copyProperties(form, postDto);
			postDto.setUserId(user.getId());

			postService.insert(postDto);
			return "redirect:/";
		} else {

			model.addAttribute("postForm", form);
			model.addAttribute("errorMessages", messages);
			return "postForm";
		}
	}



	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public String deletePost(HttpServletRequest req, Model model) {
		String postIdStr = req.getParameter("postId");
		int postId = Integer.parseInt(postIdStr);

		int count = postService.delete(postId);
		// TODO 削除できたかで分岐を追加
		return "redirect:/";
	}
}

package jp.co.board.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.board.dto.UserDto;
import jp.co.board.form.LoginFrom;
import jp.co.board.form.validationGroup.LoginFormValidation;
import jp.co.board.service.UserService;

@Controller
public class LoginController {

	@Autowired
	private UserService userService;

	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String login(Model model) {
		LoginFrom form = new LoginFrom();
		model.addAttribute("loginForm", form);
		return "login";
	}

	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String login(@ModelAttribute LoginFrom form, Model model, HttpServletRequest req) {

		List<String> messages = new ArrayList<String>();

		if (LoginFormValidation.isValid(form, messages)) {
			// フォーム情報を取得
			String loginId = form.getLoginId();
			String password = form.getPassword();

			// 特定のユーザーを取得
			UserDto userDto = userService.getUser(loginId, password);

			if (userDto != null ) {

				// ログイン情報をセッションに設定
				req.getSession().setAttribute("loginUser", userDto);
				return "redirect:/";
			} else {

				messages.add("ログインに失敗しました");
				model.addAttribute("errorMessages", messages);
				model.addAttribute("loginForm", form);
				return "login";
			}
		} else {
			model.addAttribute("errorMessages", messages);
			model.addAttribute("loginForm", form);
			return "login";
		}
	}

	@RequestMapping(value="/logout", method=RequestMethod.GET)
	public String logout( HttpServletRequest req) {
		HttpSession session = req.getSession();
		session.setAttribute("loginUser", null);
		return "redirect:/";
	}
}

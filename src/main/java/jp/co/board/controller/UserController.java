package jp.co.board.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.board.dto.BranchDto;
import jp.co.board.dto.PositionDto;
import jp.co.board.dto.UserDto;
import jp.co.board.form.UserForm;
import jp.co.board.form.validationGroup.UserFormValidation;
import jp.co.board.service.BranchService;
import jp.co.board.service.PositionService;
import jp.co.board.service.UserService;

@Controller
public class UserController {

	@Autowired
	private BranchService branchService;
	@Autowired
	private PositionService positionService;
	@Autowired
	private UserService userService;

	// 新規登録画面表示
	@RequestMapping(value = "management/user/signup", method = RequestMethod.GET)
	public String signUp(Model model) {

		// フォームの作成
		UserForm form = new UserForm();

		// フォーム用の支店・役職を取得
		setSelectList(model, "branchList", "positionList");

		model.addAttribute("userForm", form);
		return "signup";
	}

	@RequestMapping(value = "management/user/signup", method = RequestMethod.POST)
	public String signup(@ModelAttribute UserForm form, Model model ) {

		List<String> messages = new ArrayList<String>();

		//バリデーションチェック
		if (UserFormValidation.isValid(form, messages, true)) {

			UserDto userDto = new UserDto();
			BeanUtils.copyProperties(form, userDto);
			userService.insert(userDto);

			return "redirect:/managemnt";
		} else {
			// フォーム用の支店・役職を取得
			setSelectList(model, "branchList", "positionList");
			model.addAttribute("errorMessages", messages);
			model.addAttribute("userFrom", form);
			return "signup";
		}

	}

	@RequestMapping(value="/management/user/edit", method=RequestMethod.GET)
	public String update(Model model, HttpServletRequest req) {

		// id情報を取得
		String userIdStr = req.getParameter("id");
		Integer userId = Integer.parseInt(userIdStr);

		//idを元に特定のユーザーを取得
		UserForm form = new UserForm();
		UserDto dto = userService.getUserById(userId);
		BeanUtils.copyProperties(dto, form);

		// 編集に必要な情報をセット
		// フォーム用の支店・役職を取得
		setSelectList(model, "branchList", "positionList");
		model.addAttribute("userForm", form);

		return "userEdit";
	}

	@RequestMapping(value="/management/user/edit", method=RequestMethod.POST)
	public String update(@ModelAttribute UserForm form, Model model) {

		List<String> messages = new ArrayList<String>();

		if (UserFormValidation.isValid(form, messages, false)) {

			UserDto dto = new UserDto();
			BeanUtils.copyProperties(form, dto);
			userService.update(dto);
			return "redirect:/management";
		} else {

			setSelectList(model, "branchList", "positionList");
			model.addAttribute("errorMessages", messages);
			model.addAttribute("userForm", form);
			return "userEdit";
		}

	}

	@RequestMapping(value="/management/user/delete", method=RequestMethod.POST)
	public String delete(HttpServletRequest req, Model model) {

		String userIdStr = req.getParameter("userId");
		String isDeletedStr = req.getParameter("isDeleted");
		int userId = Integer.parseInt(userIdStr);
		int isDeleted = Integer.parseInt(isDeletedStr);

		// 論理削除処理
		int count =0;
		if (isDeleted == 0) {
			count = userService.delete(userId, 1);
		} else {
			count = userService.delete(userId, 0);
		}

		return "redirect:/management";
	}

	// 以下プライベートメソッド-----------------------------------

	/**
	 * @param model セッションもモデル
	 * @param branchListName 支店一覧を設定したいセッション領域名
	 * @param branchListName 役職一覧を設定したいセッション領域名
	 *
	 * セッション領域にフォームで使うselect一覧情報を設定
	 * */
	private void setSelectList(Model model, String branchListName, String positionListName) {
		List<BranchDto> branchList = branchService.getBranches();
		List<PositionDto> positionList = positionService.getPositions();

		model.addAttribute(positionListName, positionList);
		model.addAttribute(branchListName, branchList);
	}
}

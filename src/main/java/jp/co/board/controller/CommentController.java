package jp.co.board.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.board.dto.CommentDto;
import jp.co.board.form.CommentForm;
import jp.co.board.form.validationGroup.CommentValidation;
import jp.co.board.service.CommentService;

@Controller
@RequestMapping(value="/comment")
public class CommentController {

	@Autowired
	private CommentService commentService;

	@RequestMapping(value="/new", method=RequestMethod.POST)
	public String newComment(@ModelAttribute CommentForm form, Model model, HttpServletRequest req) {

		HttpSession session = req.getSession();
		List<String> messages = new ArrayList<String>();

		if (CommentValidation.isValid(form, messages)) {

			//送信された値を取得
			String userIdStr = req.getParameter("userId");
			String postIdStr = req.getParameter("postId");
			int userId = Integer.parseInt(userIdStr);
			int postId = Integer.parseInt(postIdStr);

			// データベースへ送る値を設定
			CommentDto commentDto = new CommentDto();
			BeanUtils.copyProperties(form, commentDto);
			commentDto.setUserId(userId);
			commentDto.setPostId(postId);

			//データの保存
			commentService.insert(commentDto);
			return "redirect:/";
		} else {

			session.setAttribute("errorMessages", messages);
			return "redirect:/";
		}
	}

	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public String delete(HttpServletRequest req) {
		String commentIdStr = req.getParameter("commentId");
		int commentId = Integer.parseInt(commentIdStr);

		int count = commentService.delete(commentId);
		// TODO 削除に成功したかで分岐
		return "redirect:/";
	}

}

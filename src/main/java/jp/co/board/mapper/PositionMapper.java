package jp.co.board.mapper;

import java.util.List;

import jp.co.board.entity.Position;

public interface PositionMapper {

	List<Position> getPositions();

}

package jp.co.board.mapper;

import java.util.List;

import jp.co.board.dto.CommentDto;
import jp.co.board.entity.Comment;

public interface CommentMapper {

	public int insert(CommentDto dto);
	public List<Comment> getComments();
	public int delete(int id);
}

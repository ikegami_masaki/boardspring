package jp.co.board.mapper;

import java.util.List;

import jp.co.board.entity.Branch;

public interface BranchMapper {

	// entityを返している
	List<Branch> getBranches();


}

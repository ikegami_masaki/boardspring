package jp.co.board.mapper;

import java.util.List;

import jp.co.board.dto.UserDto;
import jp.co.board.entity.User;

public interface UserMapper {

	int insert(User user);
	User getUser(String loginId, String password);
	User getUserById(int id);
	int update(UserDto dto);
	List<User> getUsers();
	int delete(int userId, int deleteNum);

}

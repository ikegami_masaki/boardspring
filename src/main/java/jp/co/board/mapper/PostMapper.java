package jp.co.board.mapper;

import java.util.List;

import jp.co.board.dto.PostDto;
import jp.co.board.entity.Post;
import jp.co.board.form.SearchForm;

public interface PostMapper {

	public int insert(PostDto dto);
	public List<Post> getPosts(SearchForm form);
	public int delete(int id);

}
